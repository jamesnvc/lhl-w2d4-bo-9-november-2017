//
//  ViewController.m
//  GesturesBreakout
//
//  Created by James Cash on 09-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *pinkView;
@property (strong, nonatomic) IBOutlet UIView *targetView;
@property (strong, nonatomic)  UIView *draggingView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // create gesture recognizer
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] init];
    // look for gestures in the pinkView
    [self.pinkView addGestureRecognizer:longPress];
    // when you recognize a gesture...
    [longPress
     // on the object self (i.e. this view controller)...
     addTarget:self
     // call the method longPressPinkView:
     // where the argument is this gesture recognizer
     action:@selector(longPressPinkView:)];

    // Target-action recap
    // [someControl addTarget:someObject action:@selector(someMethod:)]
    // this would call...
    // [someObject someMethod:someControl];

    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(pinchOnPinkView:)];
    [self.pinkView addGestureRecognizer:pinch];
}

- (void)pinchOnPinkView:(UIPinchGestureRecognizer*)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
        {
            self.draggingView = [[UIView alloc]
                                 initWithFrame:CGRectMake(0, 0, 50, 50)];
            self.draggingView.center = [sender locationInView:self.pinkView];
            [self.pinkView addSubview:self.draggingView];
            self.draggingView.backgroundColor = [UIColor yellowColor];
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            if (CGRectIntersectsRect(self.draggingView.frame, self.targetView.frame)) {
                self.draggingView.backgroundColor = [UIColor orangeColor];
                [UIView animateWithDuration:10 animations:^{
                    self.draggingView.transform = CGAffineTransformRotate(CGAffineTransformIdentity, 10000);
                }];
            } else {
                [UIView animateWithDuration:2
                                 animations:^{
                                     self.draggingView.alpha = 0;
                                 }
                                 completion:^(BOOL finished) {
                                     [self.draggingView removeFromSuperview];
                                     self.draggingView = nil;
                                 }];
            }
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            self.draggingView.center = [sender locationInView:self.draggingView.superview];
            self.draggingView.transform = CGAffineTransformScale(CGAffineTransformIdentity, sender.scale, sender.scale);
            break;
        }
    }
}

- (void)longPressPinkView:(UILongPressGestureRecognizer*)sender
{
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            self.pinkView.backgroundColor = [UIColor redColor];
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGPoint loc = [sender locationInView:self.view];
            self.pinkView.center = loc;
            break;
        }
        case UIGestureRecognizerStateEnded:
            self.pinkView.backgroundColor = [UIColor greenColor];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
